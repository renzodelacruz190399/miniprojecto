using UnityEngine;
using UnityEngine.SceneManagement;

public class Trampa : MonoBehaviour
{
   // public GameObject mensajeMuerteCanvas;

    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.CompareTag("Player"))
        {
            Debug.Log("El jugador ha colisionado con la trampa");   

            
          //  mensajeMuerteCanvas.SetActive(true);

            
            other.GetComponent<PlayerController>().enabled = false;

            RestartScene();


        }
    }

    private void RestartScene()
    {
        
        string currentSceneName = SceneManager.GetActiveScene().name;

        
        SceneManager.LoadScene(currentSceneName);
    }
}

