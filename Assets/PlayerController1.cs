using UnityEngine;

public class PlayerController1 : MonoBehaviour
{
    public GameObject congratsCanvas; // El canvas que contiene el mensaje de felicitaciones
    public Transform playerStartPosition; // La posici�n de inicio del jugador

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Finish"))
        {
            // Activar el canvas de felicitaciones cuando el jugador colisiona con el tag Finish
            congratsCanvas.SetActive(true);

            // Llamar a la funci�n para desactivar el canvas despu�s de dos segundos
            Invoke("DeactivateCanvas", 2f);

            // Mover al jugador de nuevo al inicio despu�s de dos segundos
            Invoke("ResetPlayerPosition", 2f);
        }
    }

    private void DeactivateCanvas()
    {
        // Desactivar el canvas de felicitaciones
        congratsCanvas.SetActive(false);
    }

    private void ResetPlayerPosition()
    {
        // Mover al jugador de vuelta al inicio
        if (playerStartPosition != null)
        {
            // Restablecer la posici�n y la rotaci�n del jugador
            transform.position = playerStartPosition.position;
            transform.rotation = playerStartPosition.rotation;
        }
    }
}
