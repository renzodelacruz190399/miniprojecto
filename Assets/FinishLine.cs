using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : MonoBehaviour
{
    public GameObject congratsCanvas; // El canvas que contiene el mensaje de felicitaciones
    public string nextSceneName; // El nombre de la siguiente escena a cargar

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Colisi�n con el jugador detectada.");
            // Activar el canvas de felicitaciones cuando el jugador colisiona con el tag Player
            congratsCanvas.SetActive(true);

            // Llamar a la funci�n para cargar la siguiente escena despu�s de tres segundos
            Invoke("LoadNextScene", 3f);

            
            
        }
    }

    private void LoadNextScene()
    {
        // Cargar la siguiente escena
        SceneManager.LoadScene(nextSceneName);
    }
}
