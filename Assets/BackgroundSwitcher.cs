using UnityEngine;

public class BackgroundSwitcher : MonoBehaviour
{
    public bool isRedActive;
    public bool isYellowActive;

    public GameObject backgroundRed;
    public GameObject backgroundYellow;

    void Start()
    {
        isYellowActive = true;
        isRedActive = false;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.V) && isRedActive)
        {
            backgroundRed.SetActive(false);
            backgroundYellow.SetActive(true);
            isRedActive = false;
            isYellowActive = true;
        }

        else if (Input.GetKeyDown(KeyCode.V) && isYellowActive)
        {
            backgroundRed.SetActive(true);
            backgroundYellow.SetActive(false);
            isRedActive = true;
            isYellowActive = false;
        }
    }

}


